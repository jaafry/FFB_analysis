////////////////////////////////////////////////////
//Jason Fry, September 2017
//Fantasy Football Analysis 
////////////////////////////////////////////////////

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <iostream>
#include <istream>
#include <iomanip>
#include <cstring>

#include "TSystem.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TF1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TRandom.h"

using namespace std;

int FFB_MC()
{
    //initial variables
    const int numGames = 3;
    double TSW[numGames] = {152.58,146.58,140.92};
    double F[numGames] = {96.84,111.96,105.62};
    
    //declare histograms
    TH1F *hTSW = new TH1F("","TSW",100,100,200);
    TH1F *hF = new TH1F("","F",100,80,180);
    
    //setup random variables
    TRandom *rand1 = new TRandom();
    TRandom *rand2 = new TRandom();
    
    //loop over number of games to form a histogram
    for(int j=0;j<numGames;j++) {
        hTSW->Fill(TSW[j]);
        hF->Fill(F[j]);
    }
    
    //Get Means
    double TSWSigma = hTSW->GetRMS(1);
    double FSigma = hF->GetRMS(1);
    
    //Get Std
    double TSWMean = hTSW->GetMean();
    double FMean = hF->GetMean();
    
    //Num of samples
    const int numSamples = 100000;
    
    //declare MC variables
    double FMC[numSamples] = {0.};
    double TSWMC[numSamples] = {0.};
    
    int TSWWins = 0;
    int FWins = 0;
    
    for (int i=0; i<numSamples; i++) {
        FMC[i] = rand1->Gaus(FMean,FSigma);
        TSWMC[i] = rand2->Gaus(TSWMean,TSWSigma);
        
        if (TSWMC[i] > FMC[i]) TSWWins++;
        else FWins++;
    }
    
    printf("Results for %d samples:\n\nTSW wins : %d\nF wins: %d\n",numSamples,TSWWins,FWins);
    
    return 0;
}

//////////////////////////////////////////////////
//g++ -I /Users/jasonfry/root/include/ -o FFB_MC FFB_MC.c -O -L /Users/jasonfry/root/lib/ $($ROOTSYS/bin/root-config --libs)

#ifndef __CINT__
int main() { FFB_MC(); }
#endif
